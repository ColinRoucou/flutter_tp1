import 'package:flutter/material.dart';
import 'placeholder_widget.dart';
import 'page_liste.dart';

class Home extends StatefulWidget {
 @override
 State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    LabelList(),
    PlaceholderWidget(Colors.deepOrange)
  ];

 @override
 Widget build(BuildContext context) {
   return Scaffold(
     appBar: AppBar(
       title: Text('Ma liste de course'),
     ),
 body: _children[_currentIndex], 
     bottomNavigationBar: BottomNavigationBar(
       onTap: onTabTapped, 
       currentIndex: _currentIndex, 
       items: [
         BottomNavigationBarItem(
           icon: new Icon(Icons.home),
           title: new Text('Home'),
         ),
         BottomNavigationBarItem(
           icon: new Icon(Icons.euro),
           title: new Text('euro'),
         ),
       ],
     ),
   );
 }
 void onTabTapped(int index) {
   setState(() {
     _currentIndex = index;
   });
 }
}